# Assignment 1: Komputer Store App

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Computer store application

A dynamic webpage built using vanilla JavaScript

## Table of Contents

- [Assignment 1: Komputer Store App](#assignment-1-komputer-store-app)
  - [Table of Contents](#table-of-contents)
  - [Install](#install)
  - [Usage](#usage)
  - [Maintainers](#maintainers)
  - [Contributing](#contributing)
  - [License](#license)

## Install

Host the files within the `/public` folder on a server

## Usage

Go to `{server-url:port}/index.html` using a browser. \
Try it in [GitLab Pages](https://noroff-accelerate-js.gitlab.io/assignment-1-komputer-store-app/)

## Maintainers

[Jørgen Saanum @jorgensaanum](https://gitlab.com/jorgensaanum)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Jørgen Saanum
