import { getLoan, setLoan, canLoan, hasLoan, payLoan, setBalance, getBalance, repayLoan } from './modules/bank.mjs'
import { getPay, setPay, work } from './modules/work.mjs'
import { fetchLaptopImageURL, setLaptopId, fetchLaptop, getLaptop, fetchLaptops } from './modules/store.mjs'

const onLoanClicked = (event) => {

    // Does the user already have a loan?
    if (hasLoan())
        return alert(`You already have a loan (${getLoan()} kr). Please pay back the current loan before requesting another.`)

    const userInput = prompt("How much would you like to loan?")

    const amount = parseFloat(userInput)
    // Is the input invalid? (NaN, negative, or zero)
    if (isNaN(amount) || amount <= 0)
        return alert("Invalid amount!")

    // Does the user have insufficient balance for this loan?
    if (!canLoan(amount))
        return alert("You do not have sufficient balance for this loan. You cannot get a loan more than double of your bank balance")

    // Set the loan
    setLoan(amount)

    // Add the loan amount to the balance
    setBalance(getBalance() + amount)

    // Update the HTML element holding the amount
    updateBalance()

    // Show the repay button
    updateRepayButtonVisibility()
}

const onBankClicked = (event) => {
    let pay = getPay()

    // Pay is getting transfered to bank account, or used as payment.
    setPay(0)

    // If the user has a loan, we use a fraction of the salary for payment.
    if (hasLoan()) {
        const fraction = 0.1
        payLoan(fraction * pay)
        pay *= (1 - fraction)

        // If the user just finished paying the loan, the update will hide the repay button. 
        updateRepayButtonVisibility()
    }

    // Add the pay to the balance.
    setBalance(getBalance() + pay)

    // Update the balance shown.
    updateBalance()

    // Update the pay shown.
    updatePay()
}


// Call the work function, 
// and update the DOM elements which contain the values affected
const onWorkClicked = (event) => {
    // Add 100 to pay
    work()

    // Update the pay shown
    updatePay()
}


// Call the repayLoan function, 
// and update the DOM elements which contain the values affected
const onRepayClicked = (event) => {
    // Pay the loan with the full amount of the 'pay',
    // Put the rest in the bank
    repayLoan()

    // Update the pay shown
    updatePay()

    // Update the balance shown
    updateBalance()

    // Update the repay button visibility
    updateRepayButtonVisibility()
}

const onBuyClicked = async (event) => {
    const laptop = await getLaptop()
    if (getBalance() < laptop.price)
        return alert("Insufficient balance!")

    alert("You are now the owner of this laptop!")
    setBalance(getBalance() - laptop.price)
    updateBalance()
}

// Checks the state of the loan,
// And toggles visibility depending on the state
const updateRepayButtonVisibility = () => {
    const repayButton = document.getElementById("repay-button")
    const loanButton = document.getElementById("loan-button")

    if (!repayButton.classList.contains("d-none") && !hasLoan())
        repayButton.classList.add("d-none")
    else if (repayButton.classList.contains("d-none") && hasLoan())
        repayButton.classList.remove("d-none")

    if (!loanButton.classList.contains("col-5") && hasLoan())
        loanButton.classList.add("col-5")
    else if (loanButton.classList.contains("col-5") && !hasLoan())
        loanButton.classList.remove("col-5")
}

// Updates the DOM element containing the pay to the current value
const updatePay = async () => {
    document.getElementById("pay").innerText = `${getPay()} kr`
}

// Updates the DOM element containing the balance to the current value
const updateBalance = async () => {
    document.getElementById("balance").innerText = `${getBalance()} kr`
}

// Fetches laptops from the API, and appends them as options in the dropdown
const updateLaptops = async () => {
    const laptops = await fetchLaptops()
    laptops.forEach(
        laptop => {
            const listItem = document.createElement('li')
            const button = document.createElement('button')
            button.classList.add("dropdown-item", "w-100", "text-center")
            button.innerText = laptop.title
            // When the laptop is selected in the dropdown
            button.onclick = (event) => {
                setLaptopId(laptop.id)
                updateLaptop()
                updateDropdownButton()
            }
            listItem.appendChild(button)
            document.getElementById("laptop-dropdown-list").appendChild(listItem)
        }
    )
}

// Update the store components to show the title, image, description, and specs
// of the currently selected laptop
const updateLaptop = async () => {
    const laptop = await fetchLaptop()
    const laptopUrl = await fetchLaptopImageURL(laptop.id)
    document.getElementById("laptop-image").src = laptopUrl
    document.getElementById("laptop-title").innerText = laptop.title
    document.getElementById("laptop-description").innerText = laptop.description
    document.getElementById("laptop-price").innerText = `${laptop.price} NOK`
    const featureList = document.getElementById("laptop-specs")
    featureList.innerText = ""
    laptop.specs.forEach(spec => featureList.innerText += spec + "\n")
}

// Update the dropdown with the title of the selected laptop
const updateDropdownButton = async () => {
    const laptop = await getLaptop()
    document.getElementById("laptop-dropdown-button").innerText = laptop.title
}

// Update the DOM elements once loaded
//window.addEventListener("load", updateBalance)
//window.addEventListener("load", updatePay)
//window.addEventListener("load", updateLaptops)
//window.addEventListener("load", updateLaptop)
//window.addEventListener("load", updateDropdownButton)

// Calling functions directly seems to be more reliable than
// adding them as an event listener (which often fails somehow...)
updateBalance()
updatePay()
updateLaptops()
updateLaptop()
updateDropdownButton()

// Bind onclick functions
document.getElementById("loan-button").onclick = onLoanClicked
document.getElementById("repay-button").onclick = onRepayClicked
document.getElementById("bank-button").onclick = onBankClicked
document.getElementById("work-button").onclick = onWorkClicked
document.getElementById("laptop-buy-button").onclick = onBuyClicked