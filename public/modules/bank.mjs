import { setPay, getPay } from './work.mjs'

let balance = 1000
let loan = 0

export const setLoan = (amount) => loan = amount

export const getLoan = () => loan

export const hasLoan = () => loan > 0

export const canLoan = (amount) => amount <= balance * 2

export const payLoan = (amount) => {
    // If the payment is less than the current loan, we subtract from loan normally.
    if (amount < loan) {
        setLoan(loan - amount)
    }
    // If the loan less than the payment, we send the rest of the payment to balance.
    else {
        setBalance(balance + (amount - loan))
        setLoan(0)
    }

}

export const repayLoan = () => {
    // If the pay is less than the current loan, 
    // we subtract the pay from the loan and set pay to 0.
    if (getPay() < loan) {
        setLoan(loan - getPay())
        setPay(0)
    }
    // If the loan less than the pay, 
    // we set loan and pay to 0,
    // and send the rest of the payment to balance.
    else {
        setBalance(balance + (getPay() - loan))
        setLoan(0)
        setPay(0)
    }
}

export const setBalance = (amount) => balance = amount

export const getBalance = () => balance