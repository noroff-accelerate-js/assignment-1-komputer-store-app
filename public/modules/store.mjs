const url = "https://noroff-komputer-store-api.herokuapp.com"

let laptopIdSelected = 1

export const fetchLaptops = async () => fetch(`${url}/computers`).then(response => response.json())

let laptops = await fetchLaptops()

export const getLaptop = async (id = laptopIdSelected) => laptops.find(laptop => laptop.id === id)

export const fetchLaptop = async (id = laptopIdSelected) => fetch(`${url}/computers/${id}`).then(response => response.json())

export const fetchLaptopImageURL = async (id) => fetch(`${url}/computers/${id}`)
    .then(response => response.json())
    .then(laptop => `${url}/${laptop.image}`)
    // Fix for the 5th laptop image url, should ideally be fixed on the backend
    // Switch the url extension to .png, 
    // since the API returns the image url with the wrong extension
    .then(url => id === 5 ? url.replace(".jpg", ".png") : url)

export const getLaptopId = () => laptopIdSelected

export const setLaptopId = (id) => laptopIdSelected = id


